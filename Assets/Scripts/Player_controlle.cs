﻿using UnityEngine;
using System.Collections;

public class Player_controlle : MonoBehaviour
{

    public float walkSpeed = 5;
    public float jump;

    Animator anim;

    bool walking = false;
    bool jumping = false;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (walkSpeed >= 0.1)
            {
                anim.SetBool("Walk", walking);
            }
            else
            {
                walking = false;
            }
           
            GetComponent<Rigidbody2D>().velocity = new Vector2(walkSpeed, GetComponent<Rigidbody2D>().velocity.y);
            transform.localRotation = Quaternion.Euler(0, 0, 0);
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (walkSpeed >= 0.1)
            {
                anim.SetBool("Walk", walking);
            }
            else
            {
                walking = false;
            }

            GetComponent<Rigidbody2D>().velocity = new Vector2(-walkSpeed, GetComponent<Rigidbody2D>().velocity.y);
            transform.localRotation = Quaternion.Euler(0, 180, 0);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (jump >= 0.1)
            {
                anim.SetBool("Jump", jumping);
            }
            else
            {
                jumping = false;
            }

            GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, jump);
        }
    }
}

